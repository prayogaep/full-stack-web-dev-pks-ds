<?php

class Buah {
    public $nama;
    public $berat;

    public function __construct($nama, $berat)
    {
        $this->nama = $nama;
        $this->berat = $berat;

    }   

    public function tampilkan()
    {
        return "Buah ini adalah buah " . $this->nama . " dengan berat " . $this->berat . " gram.";
    }

    public function __destruct()
    {
        echo 'Buah hapus dari memory';
    }
}

$mangga = new Buah('mangga', 100);
echo  $mangga->tampilkan();